
public class Loops {
	public static void main(String[] args) {
		
		
		for (int i=1;i<=10;i++) {
			System.out.println(i);
			}
		
		System.out.println("END of For Loop");
		
		int count=1;
		
		while(count <=10){
			System.out.println(count);
			count ++;
		}
		
		/**/
		
		System.out.println("END OF WHILE LOOP");
		int action= 1;
		
		do {
			System.out.println(action);
			action ++;
		}while(action <=10);
		
	}

}
