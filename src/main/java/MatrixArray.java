
public class MatrixArray {

	public static void main(String[] args) {
		
		//Declare  Array
		
		int[][] twoDim;
		
		//create
		
		twoDim = new int[5][];
		
		//initialize
		
		twoDim[0]= new int[]{1};
		twoDim[1]= new int[] {1,2};
		twoDim[2]=new int[] {1,2,3};
		twoDim[3]= new int[] {1,2,3,4};
		twoDim[4]= new int[] {1,2,3,4,5};
		
		
		
		for (int i=0;i<twoDim.length;i++) {
			//in each array
			
			for (int j = 0; j < twoDim [i].length; j++) {
				
				System.out.print(twoDim[i][j] +" "); //this will make print in vertical way
				
			}
			
			System.out.println();
		}
		
	}

}
